# Maintainer: Svyatoslav Ryhel <clamor95@gmail.com>
# Co-Maintainer: Maxim Schwalm <maxim.schwalm@gmail.com>

pkgname=device-asus-tf700t
pkgdesc="Asus Transformer Infinity TF700T"
pkgver=2
pkgrel=1
url="https://postmarketos.org"
license="MIT"
arch="armv7"
options="!check !archcheck"
depends="
	alsa-ucm-conf
	asus-transformer-blobtools
	libvdpau-tegra
	linux-postmarketos-grate
	mesa-dri-gallium
	mkbootimg
	postmarketos-base
"
makedepends="devicepkg-dev"
source="
	deviceinfo
	rootston.ini
"
subpackages="
	$pkgname-nonfree-firmware:nonfree_firmware
	$pkgname-phosh
	$pkgname-x11
"

build() {
	devicepkg_build $startdir $pkgname
}

package() {
	devicepkg_package $startdir $pkgname
}

phosh() {
	install_if="$pkgname=$pkgver-r$pkgrel postmarketos-ui-phosh"
	install -Dm644 "$srcdir"/rootston.ini \
		"$subpkgdir"/etc/phosh/rootston.ini
}

nonfree_firmware() {
	pkgdesc="Asus Transformers WiFi & BT firmware"
	depends="firmware-asus-transformer"
	mkdir "$subpkgdir"
}

x11() {
	install_if="$pkgname=$pkgver-r$pkgrel xorg-server"
	depends="xf86-video-opentegra"
	mkdir "$subpkgdir"
}

sha512sums="
6b6839d5a965a0b10667541115fe762a7cc28818d53bb1102847db4694a7ddca8977c5962f256cc302f69a7144ea952db22b8844f7da5ad07382ffa86f8ff22b  deviceinfo
c15e0d54e311892556cf9447d8431b6239fcce29bb011bf4931c909b4d59af2f00b87637b0eb67554ccd063f569ad24c868433c551e27354b3b9af604a08f3d8  rootston.ini
"
